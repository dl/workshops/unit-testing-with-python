<!--
author:   Frank Lange
          Jonathan Hartman

email:    flange@itc.rwth-aachen.de
          hartman@itc.rwth-aachen.de

version:  0.0.1

language: en

comment:  Workshop intended to cover

script:   https://cdn.jsdelivr.net/pyodide/v0.24.0/full/pyodide.js

import: https://raw.githubusercontent.com/LiaTemplates/Pyodide/master/README.md

@onload: window.py_packages = ["pytest"]
-->

``` python @Pyodide.exec
class SimpleTestFramework:
    def __init__(self):
        self.tests = []

    def add_test(self, test_func):
        self.tests.append(test_func)

    def run_test(self, test_name):
        for test_func in self.tests:
            if test_func.__name__ == test_name:
                try:
                    test_func()
                    return f"Test '{test_name}' passed."
                except AssertionError as e:
                    return f"Test '{test_name}' failed: {str(e)}"
        return f"Test '{test_name}' not found."

test_framework = SimpleTestFramework()
```

# Introduction to Unit Testing with Python

## Recapping the Pre-Workshop Task

What sorts of challenges or errors did you think about when you were implementing the validation method?

    [[___]]

When adding or changing the code, how did you check that it was working correctly?

    [[___]]

How sure are you that you've covered all possible inputs a user might have?

- [(1 total)] I'm pretty sure I covered all possibilities
- [(2 somewhat)] I can think of a couple ways my code might break
- [(3 not really)] I have a couple checks, but just for obvious things
- [(4 none)] I don't really have any checks other than basic ones

Can you identify any potential bugs in the current code? How would you go about debugging them?

    [[___]]

If you were to refactor or modify portions of this code, how would you ensure that you do not introduce new bugs or break existing functionality?

    [[___]]

## Introduction to Unit Testing

### Why do Unit Testing?

Unit testing is a crucial component of general software development.

#### Example: Catching a Bug Early

Say we are in the middle of writing a portion of a much larger project related to interpreting and
checking architectural drawings. As part of the code we are working on, we write a function to
calcluate the angles of a right triangle:

``` python @Pyodide.exec
import math

def calculate_right_triangle_angles(leg1, leg2):
    angle1 = math.degrees(math.atan(leg2 / leg1))
    angle2 = 90 - angle1
    angle3 = 90.0

    return round(angle1, 2), round(angle2, 2), round(angle3, 2)

def run_test(function, *args, expected):
    try:
        result = function(*args)
        test_result = "PASS" if result == expected else f"FAIL (expected: {expected}, result: {result})"
    except Exception as err:
        test_result = f"FAIL: Test raised the following exception: {err}"

    print(test_result)
```

``` python
def calculate_right_triangle_angles(leg1, leg2):
    angle1 = math.degrees(math.atan(leg2 / leg1))
    angle2 = 90 - angle1
    angle3 = 90.0

    return round(angle1, 2), round(angle2, 2), round(angle3, 2)
```

##### Writing Tests

To check that this test works, we might write a couple of tests. We're not going to get into test
frameworks quite yet, so I've just written a simple little function that works similarly - we have a
function called "run_test" that takes a function, then the arguments that we want to pass into the
function, and finally the expected value:

``` python
# Check that the angles of a equilateral triangle are correctly calculated
print("Correctly calculate right triangle angles")
run_test(calculate_right_triangle_angles, 5, 5, expected=(45.0, 45.0, 90.0))

# Check that the angles of a 3-4-5 triangle are correctly calculated
print("Correctly calculate 3-4-5 triangle angles")
run_test(calculate_right_triangle_angles, 3, 4, expected=(53.13, 36.87, 90.0))
```
@Pyodide.eval

So our math checks out, but are there any inputs that might make give this functions issues? Try
updating the cell below to see if you can get the function to throw some errors:

``` python
# Put different values in place of <value1>, <value2> to see if you can get
calculate_right_triangle_angles(<value1>, <value2>)
```
@Pyodide.eval

What Errors were you able to make appear?

        [[___]]

##### Predicting Bugs

There are a number of possible things we could check for, so there aren't really wrong answers. My
thought is that we can possibly end up with a `ZeroDivisionError` if the first value is a 0:

``` python
calculate_right_triangle_angles(0, 4)
```
@Pyodide.eval

So we might write a test that checks this. We can argue that if someone says the triangle has sides
of length 0 and 4, that the angles are actually 90/0/90:

``` python
# Check that we can caculate the angles of a triangle with a 1st side of 0
print("Correctly calculate triangle with a first side of length 0")
run_test(calculate_right_triangle_angles, 0, 4, expected=(90.0, 0.0, 90.0))
```
@Pyodide.eval

Now that test still doesn't pass because we haven't updated our code yet. Let's do that now and
then run our test:

``` python
def calculate_right_triangle_angles(leg1, leg2):
    if leg1 == 0:
        return 90.0, 0.0, 90.0

    angle1 = math.degrees(math.atan(leg2 / leg1))
    angle2 = 90 - angle1
    angle3 = 90.0

    return round(angle1, 2), round(angle2, 2), round(angle3, 2)

print("Correctly calculate triangle with a first side of length 0")
run_test(calculate_right_triangle_angles, 0, 4, expected=(90.0, 0.0, 90.0))
```
@Pyodide.eval

<section>


#### 2

Once you submit your answer, you'll see some of mine...


- [ ] It's possible to have a division by 0 if the variable "leg1" is 0
- [ ] It's possible to provide a negative length for either leg
- [ ] We are assuming that leg1 and leg2 are numbers, but what if we get a different type?




---

``` python
def add_two_numbers(a: int, b: int) -> int:
    return a + b

# Can we add two numbers together and get the correct value
result = add_two_numbers(4, 7)
test_result = "PASS" if result == 11 else "FAIL"
print(f"{test_result}: Correct result: 11, Test Result: {result}.")

# Can we add two float values together and get the correct value
result = add_two_numbers(1.3, 6.5)
test_result = "PASS" if result == 7.8 else "FAIL"
print(f"{test_result}: Correct result: 7.8, Test Result: {result}.")

# What happens when we add a number to something that isn't a number?
# Can we add two numbers together and get the correct value
try:
    result = add_two_numbers(4, "foo")
except TypeError:
    print("PASS: Test correctly raises a TypeError")
except Exception as err:
    print(f"FAIL: Incorrect Error Raised: {type(err)}: {err}")
else:
    print("FAIL: No Error was Raised")
```
@Pyodide.eval

## What does a Unit Test look like?

There are several options for writing unit tests. Two of the most commonly used are `unittest` and
`pytest`. Both offer integration with most modern IDEs and automatic test discovery, and up to
personal preference. We will stick to pytest for our examples, but to give a few pros / cons of
each:

A Brief Comparison of Pytest and Unittest
-----------------------------------------

`unittest`

- **Part of the Python Standard Library**

  - doesn't require any additonal libraries to function

- **Highly Structured**

  - encourages structuring tests into classes and methods
  - clearly defined assertion methods like "assertEqual" and "assertTrue"

`pytest`

- **External Dependency**

  - must be installed before it can be used
  - install with `pip install pytest`

- **Flexible**

    - Paramaterizing, testing fixtures and mocking

### A Unittest Example

my_script.py
------------


test_my_script.py
-----------------

``` python
import unittest
from my_script import add_two_numbers

class TestMathOperations(unittest.TestCase):

    def test_add_two_numbers(self):
        # Check that we can correctly add two numbers
        self.assertEqual(add_two_numbers(1, 2), 3)

if __name__ == '__main__':
    unittest.main()
```
@Pyodide.eval

### A Pytest Example

my_script.py
------------



test_my_script.py
-----------------

``` python
@test_framework.add_test
def test_add_two():
    assert add_two(3, 4) == 7

@test_framework.add_test
def test_multiply():
    assert multiply(2, 3) == 6

# Run a specific test
result = test_framework.run_test("test_add_two")
print(result)

result = test_framework.run_test("test_multiply")
print(result)
```
@Pyodide.eval

### Extending Tests

We can incorporate multiple checks into a single test. Ideally these are all related to the test
description. For example, we might add a few more assertions to our `test_add_two_numbers` test
to look at a few different aspects of adding numbers

``` python
def test_add_two_numbers():
    # Check that we can correctly add two numbers
    assert add_two_numbers(1, 2) == 3

    # Check that we can add a negative number
    assert add_two_numbers(-3, 5) == 2

    # Check that we can add two zeros
    assert add_two_numbers(0, 0) == 0

    # Check that we can add numbers with decimals
    assert add_two_numbers(1.2, 3.4) == 4.6

    # Check that we can add large numbers
    assert add_two_numbers(1234, 5678) == 6912
```

We might have other aspects we would like to test, however. What should happen if one of the values
we recieve in the test is not a number? We can plan this out ahead of time in our test:

``` python
import pytest
from my_script import add_two_numbers

def test_add_two_numbers_invalid_input():
    # Check that we raise a ValueError if we provide a non numeric value
    with pytest.raises(ValueError):
        add_two_numbers("foo", 2)
```

We use a context manager here that is provided by pytest: `with pytest.raises`. This is similar to
the asserts, but instead of checking that the returned value is True, it checks to see if a
particular error is raised. If the error does *not* appear, that would be a test failure.

## Test-Driven Development

You may have noticed that in the previous section we had written a test for which
