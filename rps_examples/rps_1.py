# Define the winning conditions
win_conditions = {
    'rock': {'scissors'},
    'paper': {'rock'},
    'scissors': {'paper'}
}

# Get the player choices
player1_choice = input("Player 1: ").lower()
player2_choice = input("Player 2: ").lower()

# Determine the winner
print("--------------------")
if player1_choice == player2_choice:
    print("It's a tie!")
elif player2_choice in win_conditions[player1_choice]:
    print("Player 1 wins!")
else:
    print("Player 2 wins!")