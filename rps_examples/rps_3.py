def get_player_choice(player: str) -> str:
    """
    Get the player's choice of rock, paper, or scissors.

    Will continue to prompt the player until a valid choice is entered.

    Args:
        player (str): The player's name.

    Returns:
        str: The player's choice.
    """
    while True:
        try:
            choice = input(f"{player}: ").lower()
            if choice not in ['rock', 'paper', 'scissors']:
                raise ValueError("Invalid choice. Please choose rock, paper, or scissors.")
            return choice
        except ValueError as e:
            print(e)

def determine_winner(player1_choice: str, player2_choice: str) -> int:
    """
    Given two player choices, determine the winner. The return value will be -1 if player 1 wins,
    1 if player 2 wins, or 0 if it's a tie.

    Args:
        player1_choice (str): Player 1's choice.
        player2_choice (str): Player 2's choice.

    Returns:
        int: -1 if player 1 wins, 1 if player 2 wins, or 0 if it's a tie.
    """
    # Define the winning conditions
    win_conditions = {
        'rock': {'scissors'},
        'paper': {'rock'},
        'scissors': {'paper'}
    }

    if player1_choice == player2_choice:
        # It's a tie
        return 0
    elif player2_choice in win_conditions[player1_choice]:
        # Player 1 wins
        return -1
    else:
        # Player 2 wins
        return 1

# Get the player choices
player1_choice = get_player_choice("Player 1")
player2_choice = get_player_choice("Player 2")

# Determine the winner
print("--------------------")
winner = determine_winner(player1_choice, player2_choice)
winner_text = {
    -1: "Player 1 wins!",
    0: "It's a tie!",
    1: "Player 2 wins!"
}
print(winner_text[winner])