from itertools import combinations

def get_player_choice(player: str) -> str:
    """
    Get the player's choice of rock, paper, or scissors.

    Will continue to prompt the player until a valid choice is entered.

    Args:
        player (str): The player's name.

    Returns:
        str: The player's choice.
    """
    while True:
        try:
            choice = input(f"{player}: ").lower()
            if choice not in ['rock', 'paper', 'scissors', 'lizard', 'spock']:
                raise ValueError("Invalid choice. Please choose rock, paper, or scissors.")
            return choice
        except ValueError as e:
            print(e)

def determine_winner(player1_choice: str, player2_choice: str) -> int:
    """
    Given two player choices, determine the winner. The return value will be -1 if player 1 wins,
    1 if player 2 wins, or 0 if it's a tie.

    Args:
        player1_choice (str): Player 1's choice.
        player2_choice (str): Player 2's choice.

    Returns:
        int: -1 if player 1 wins, 1 if player 2 wins, or 0 if it's a tie.
    """
    # Define the winning conditions
    win_conditions = {
        'rock': {'scissors', 'lizard'},
        'paper': {'rock', 'spock'},
        'scissors': {'paper', 'lizard'},
        'lizard': {'paper', 'spock'},
        'spock': {'scissors', 'rock'}
    }

    if player1_choice == player2_choice:
        # It's a tie
        return 0
    elif player2_choice in win_conditions[player1_choice]:
        # Player 1 wins
        return -1
    else:
        # Player 2 wins
        return 1

# Get the number of players
num_players = int(input("How many players? "))

player_choices = {}
for player in range(1, num_players + 1):
    player_choices[player] = get_player_choice(f"Player {player}")

# Get all possible pairings of players
player_pairings = combinations(player_choices.keys(), 2)

# Determine the winner of each pairing
pairing_winners = {}
for player1, player2 in player_pairings:
    player1_choice = player_choices[player1]
    player2_choice = player_choices[player2]
    pairing_winners[(player1, player2)] = determine_winner(player1_choice, player2_choice)

# Determine the winner of the game
player_scores = {}
for player in player_choices.keys():
    player_scores[player] = 0

for (player1, player2), winner in pairing_winners.items():
    if winner == -1:
        player_scores[player1] += 1
    elif winner == 1:
        player_scores[player2] += 1

# Print the results
print("--------------------")
for player, score in player_scores.items():
    print(f"Player {player} score: {score}")

# Get the max score
max_score = max(player_scores.values())

# Get the winners
winners = [player for player, score in player_scores.items() if score == max_score]

# Print the winners
if len(winners) == 1:
    print(f"Player {winners[0]} wins!")
else:
    print("Multiple players have the same score!")
    print("The winners are:")
    for winner in winners:
        print(f"Player {winner}")