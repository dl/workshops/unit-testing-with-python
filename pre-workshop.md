<!--
author:   Frank Lange
          Jonathan Hartman

email:    flange@itc.rwth-aachen.de
          hartman@itc.rwth-aachen.de

version:  0.0.1

language: en

comment:  Workshop intended to cover
-->

# Introduction to Unit Testing with Python

... Stuff about what unit testing is and why you should do it ...

## Pre-Workshop Tasks

Before we start the workshop, we would like you to spend a little time (no more than ~30 minutes)
on the following tasks:

### Task 1 - Rock Paper Scissors

> Prompt:
>
> Start by writing a simple python script that accepts the results of a Rock, Paper, Scissors game.
> Your script should accept two user inputs, one for player 1 and one for player 2, then determine the winner.
> For now, ignore input validataion and avoid writing functions.

Your output might look a little like this:

``` python
>>> Player 1: rock
>>> Player 2: paper
>>> --------------------
>>> Player 2 wins!
```

If you get stuck, here's a hint:

<Figure out how to get hints on click or something>

You can also peek at a completed version of this task [here](www.google.com).

### Task 2 - Input Validation

> Prompt:
>
> Now lets make out script a little more complicated by adding a method. We said you should ignore
> input validation in the last task, but you've probably noticed that if you accept just any input,
> it becomes difficult to determine a winner, or at least you had to include a case to check if
> either input was not "rock", "paper" or "scissors". Let's write a function now to ask for user
> input that will tell the user if their input is not valid and ask again.

It might look something like this:

``` python
>>> Player 1: foo
>>> Invalid choice. Please choose rock, paper, or scissors.
>>> Player 1: bar
>>> Invalid choice. Please choose rock, paper, or scissors.
>>> Player 1: rock
>>> Player 2: buzz
>>> Invalid choice. Please choose rock, paper, or scissors.
>>> Player 2: paper
>>> --------------------
>>> Player 2 wins!
```

If you get stuck, here's a hint:

<Figure out how to get hints on click or something>

You can also peek at a completed version of this task [here](www.google.com).

### Task 3 - Adding Functions

> Prompt:
>
> Next, we would like to add a new function that will help us determine which player won the game.
> You will already have some code like this in your script, but we have some specifications for it
> this time. The function signature should look like this:
>
> `determine_winner(player1_choice: str, player2_choice: str) -> int:`
>
> The function should return the integer -1 if player 1 won, 1 if player 2 won, and 0 if there was
> a tie.
>
> The output of your function should not change from the previous task.

If you get stuck, here's a hint:

<Figure out how to get hints on click or something>

You can also peek at a completed version of this task [here](www.google.com).

### Task 4 - Extend the choices

> Prompt:
>
> We want to extend the possible choices in our game to let us play [Rock, Paper, Scissors, Lizard, Spock](https://www.instructables.com/How-to-Play-Rock-Paper-Scissors-Lizard-Spock/).
> We'll need to extend both our choice validation code as well as our`determine_winner` function to
> account for these new options.

Example Output:

``` python
>>> Player 1: spock
>>> Player 2: paper
>>> --------------------
>>> Player 2 wins!
```

### Task 5 - Multiple Players

> Prompt:
>
> Finally, let's add in the ability to have more than one player at a time. We should be able to
> enter the moves of each player in turn, and each player will have his move calculated against
> every other player. Whichever player or players has the most wins against all other players
> is the winner.
>
> You can use the "combinations" method from the "itertools" library to help with this. It will
> give you every possible permutation of a list of items:
``` python
from itertools import combinations

my_list = ["foo", "bar", "fizz", "buzz"]
print(list(combinations(my_list, r=2)))
>>> [('foo', 'bar'), ('foo', 'fizz'), ('foo', 'buzz'), ('bar', 'fizz'), ('bar', 'buzz'), ('fizz', 'buzz')]
```

Example Output:

``` python
>>> How many players? 5
>>> Player 1: lizard
>>> Player 2: lizard
>>> Player 3: scissors
>>> Player 4: paper
>>> Player 5: spock
>>> --------------------
>>> Player 1 score: 2
>>> Player 2 score: 2
>>> Player 3 score: 3
>>> Player 4 score: 1
>>> Player 5 score: 1
>>> Player 3 wins!
```